#!/bin/bash
#
#    Copyright © 2016, 2018 Zetok Zalbavar
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Script for merging merge requests. Works only when there are no merge
# conflicts. Assumes that working dir is a git repo.
#
# Requires SSH key that gitlab accepts and GPG set to sign merge commits.

# usage:
#   ./$script $mr_number $optional_message
#
#
# $pr_number – number of the MR as shown on GL
# $optional_message – message that is going to be put in merge commit,
#       before the appended shortlog.
#

PR=$1

# make sure to add newlines to the message, otherwise merge message
# will not look well
if [[ ! -z $2 ]]
then
    OPT_MSG="
$2
"
fi


# check if supplied var is a number
if [[ ! "${PR}" =~ ^[[:digit:]]+$ ]]
then
    echo "Not a MR number!" && \
    exit 1
fi

# print the message only if the merge was successful
after_merge_msg() {
    echo ""
    echo "MR !$PR was merged into «merge$PR» branch."
    echo "To compare with master:"
    echo "git diff master..merge$PR"
    echo ""
    echo "To push that to master on gitlab:"
    echo "git checkout master && git merge --ff merge$PR && git push origin master"
    echo ""
    echo "After pushing to master, delete branches:"
    echo ""
    echo "git branch -d {merge,}$PR"
}


git fetch origin refs/merge-requests/$PR/head:$PR && \
git checkout master -b merge$PR && \
git merge --no-ff -S $PR -m "Merge request !$PR
$OPT_MSG
$(git shortlog master..$PR)" && \
after_merge_msg
