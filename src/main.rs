/*
    Copyright © 2018-2020 Zetok Zalbavar <zexavexxe@gmail.com>

    This file is part of ods2sql.

    ods2sql is libre software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    ods2sql is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with ods2sql.  If not, see <https://www.gnu.org/licenses/>.
*/


//! This program creates a sqlite3 database from an ODS spreadsheet.


#![deny(missing_docs)]

#[macro_use]
extern crate clap;
#[macro_use]
extern crate log;

use calamine::{
    DataType,
    Ods,
    Reader,
    open_workbook,
};
use clap::Arg;
use rusqlite::{Connection, NO_PARAMS};

use std::path::{Path, PathBuf};
use std::process;


/// File name extension used for the sqlite database file.
const DB_EXTENSION: &str = "sqlite";

fn init_logger() {
    env_logger::Builder::from_default_env()
        .format_timestamp(None)
        .init();
}

fn main() {
    init_logger();

    let m = app_from_crate!()
        .about(crate_description!())
        .long_about(format!(
            "{desc}\n\n\
             Default output sqlite file is created based on input file with the\n\
             extension changed from '.ods' to '.{ext}'.  If output file already\n\
             exists {name} terminates.\n\n\
             Assumptions made by {name}:\n  \
             * values in the first row are column names\n  \
             * all cell contents are passed as 'TEXT' SQL type",
            desc=crate_description!(),
            ext=DB_EXTENSION,
            name=crate_name!()
        ).as_str())
        .arg(Arg::new("INPUT FILE")
             .about("Sets the input file to use.")
             .required(true)
             .index(1))
        .arg(Arg::new("OUTPUT FILE")
             .short('o')
             .long("output")
             .required(false)
             .takes_value(true)
             .about("Sets custom output filename.")
             .long_about(&format!(
                 "Sets custom output filename.\n\n\
                 Default output filename is the same as input filename with\n\
                 extension changed from '.ods' to '.{}'.", DB_EXTENSION)))
        .get_matches();

    let ods_path = m.value_of("INPUT FILE").unwrap_or_else(|| {
        error!("Missing INPUT FILE, exiting.");
        process::exit(1);
    });
    let ods_path = Path::new(ods_path);

    if !ods_path.is_file() {
        error!("Supplied path is not a file.");
        process::exit(1);
    }

    let sqlite_file = match m.value_of("OUTPUT FILE") {
        Some(f) => PathBuf::from(f),
        None => ods_path.with_extension(DB_EXTENSION),
    };
    if sqlite_file.exists() {
        error!("Sqlite output file already exists!");
        process::exit(1);
    }

    // create output sqlite DB file
    let mut db = Connection::open(sqlite_file).map_err(|e| {
        error!("Couldn't create a new sqlite DB file: {}", e);
        process::exit(1);
    }).unwrap();


    let mut workbook: Ods<_> = open_workbook(ods_path).map_err(|e| {
        error!("Couldn't open input file: {}", e);
        process::exit(1);
    }).unwrap();

    let sheets = workbook.sheet_names().to_owned();

    trace!("Sheets: {:?}", sheets);

    for sheet in sheets {
        let range = workbook.worksheet_range(&sheet).unwrap_or_else(|| {
            error!("Couldn't unwrap range of sheet '{}'", &sheet);
            process::exit(1);
        }).map_err(|e| {
            error!("Couldn't get range of sheet '{}': {}", &sheet, e);
            process::exit(1);
        }).unwrap();

        let mut rows_iter = range.rows();

        // TODO: change to a fn outside of main()
        // get column names, and if they are not a String, use column number
        let columns = rows_iter.next().unwrap_or_else(|| {
            error!("Couldn't get column names from sheet '{}'", sheet);
            process::exit(1);
        }).iter().enumerate()
            .map(|(n, col)| {
                (n, try_get_string(col).unwrap_or_else(|_| {
                    warn!("DataType of the column name cell in sheet \
                           '{}' is not a String: {:?}\n\
                           Using column number instead of the name: {}",
                          sheet, col, n);
                    format!("column_{}", n)
                }))
            })
            // make sure that there are no duplicate column names
            .fold(Vec::new(), |mut vec, (n, colname)| {
                add_unique_string(&mut vec, colname, &format!("{}", n));
                vec
            });
        trace!("Columns of '{}': {:?}", &sheet, &columns);

        // start DB transaction to speed up adding rows
        let tx = db.transaction().map_err(|e| {
            error!("DB: Couldn't start transaction for table '{}': {}",
                   &sheet, e);
            process::exit(1);
        }).unwrap();

        let create_table = &create_table_string(&sheet, &columns);
        tx.execute(create_table, NO_PARAMS).map_err(|e| {
            error!("DB: Couldn't create table '{}': {}", &sheet, e);
            process::exit(1);
        }).unwrap();


        for row in rows_iter {
            let mut insert_values = Vec::with_capacity(columns.len());
            for row_cell in row {
                let cell_string = match *row_cell {
                    DataType::Int(ref i) => Some(format!("{}", i)),
                    DataType::Float(ref f) => Some(format!("{}", f)),
                    DataType::String(ref s) => Some(s.clone()),
                    // ↓ true | false
                    DataType::Bool(b) => Some(format!("{}", b)),
                    DataType::Error(ref e) => {
                        error!("Error in sheet '{}', row '{:?}': {:?}",
                               sheet, row, e);
                        None
                    },
                    DataType::Empty => None,
                };

                insert_values.push(cell_string);
            }
            assert_eq!(insert_values.len(), columns.len());
            trace!("Insert values for SQL statement:\n{:?}", insert_values);

            let ins_stmt = create_insert_stmt(&sheet, &columns);
            tx.execute(&ins_stmt, &insert_values).map_err(|e| {
                error!("DB: Couldn't insert statement into table '{}': {}",
                       &sheet, e);
                debug!("DB: Insert stmt:\n{}", &ins_stmt);
                process::exit(1);
            }).unwrap();
            trace!("DB: executed: `{}`", &ins_stmt);
        }

        // don't forget to commit, otherwise data would get dropped
        tx.commit().map_err(|e| {
            error!("DB: couldn't commit transaction for table '{}': {}",
                   &sheet, e);
            process::exit(1);
        }).unwrap();
    }

    info!("Created Sqlite DB from ODS.");
}



/// Sanitize strings that need to be used in SQL statements verbose.
fn sanitize_str(s: &str) -> String {
    s.replace("'", "''")
}


// TODO: add support for column types other than `TEXT`
fn create_table_string(table: &str, columns: &[String]) -> String {
    let mut create_table = format!(
        "CREATE TABLE '{}' (\n  \
         id    INTEGER PRIMARY KEY AUTOINCREMENT",
        sanitize_str(table)
    );

    for column in columns {
        create_table += ",\n";
        create_table += &format!("  '{}'    TEXT", sanitize_str(column));
    }
    create_table += "\n);";
    trace!("Whole SQL statement for creating tables for sheet '{}':\n{}\n",
           table, &create_table);

    create_table
}

/**
Adds to the `Vec` unique `String`.

In case where `String` is already in the `Vec`, it's extended with
supplied `&str` ad infinitum until it becomes unique.
*/
fn add_unique_string(v: &mut Vec<String>, s: String, u: &str) {
    if v.contains(&s) {
        info!("Duplicate string '{}', appending unique '_{}'",
              s, u);
        add_unique_string(v, format!("{}_{}", s, u), u);
    } else {
        v.push(s);
    }
}

/**
Create a String used for insert statement

Creates something that looks ~like that:

```sql
INSERT INTO '$table_name' ($('column'),+) VALUES ($(?),+);
```
*/
fn create_insert_stmt(table: &str, columns: &[String]) -> String
{
    /// Remove unnecessary `, ` at the end of the String.
    fn rmc(s: &mut String) {
        if s.ends_with(", ") {
            s.pop();
            s.pop();
        }
    }


    trace!("Insert stmt columns: `{:?}`", columns);

    let mut stmt = format!("INSERT INTO '{}' (", sanitize_str(table));
    for column in columns {
        stmt += &format!("'{}', ", sanitize_str(column));
    }

    rmc(&mut stmt);
    stmt += ") VALUES (";

    for _ in 0..columns.len() {
        stmt += "?, ";
    }

    rmc(&mut stmt);
    stmt += ");";
    stmt
}

/**
Try to obtain `String` column name from the cell, or return an error if
the data type is not a `String`.
 */
fn try_get_string(cell: &DataType) -> Result<String, ()> {
    match *cell {
        DataType::String(ref s) => Ok(s.clone()),
        _ => Err(()),
    }
}
