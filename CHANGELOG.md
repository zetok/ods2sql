# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic
Versioning](https://semver.org/spec/v2.0.0.html).


## v0.4.0 - 2020-10-24

### Changed

- LTO is now used for the release builds, resulting in a smaller binary size
  at the cost of an increased compile time.
- Short `-h` now will provide only short description of the program. For the
  full description use `--help`.

## v0.3.1 - 2020-10-21

### Changed

- [License](/LICENSE) was changed from GPLv3+ to AGPLv3+.
- Update of `env_logger` crate caused some cosmetic changes of error/debug
  messages.

## v0.3.0 - 2019-03-13

### Changed

- Boolean values are now converted to `true` / `false` `TEXT` SQLite type
  instead of `TRUE` / `FALSE` `TEXT`.
- Empty cells and errors when trying to get cell content are now converted to
  `NULL` SQLite value instead of an empty string.

## v0.2.0 - 2019-01-24

### Added

- This changelog.
- Flag `-o`/`--output` to specify custom output filename.

### Changed

- Update of `env_logger` crate caused a cosmetic change in the error output
  format.

### Fixed

- Fixed a possible issue with usage of non-escaped table and column names in
  SQLite statements, which could cause program to fail to process all data.
- Updated dependencies to latest available versions, and the updated versions of
  those libraries might contain bugfixes affecting this program.

## v0.1.1 - 2018-09-08

### Fixed

- Fixed possible future compiling compatibility problem with SQLite wrapper.

## v0.1.0 - 2018-08-17

### Added

- First working release.
