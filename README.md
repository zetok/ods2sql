# ods2sql

[![license](https://img.shields.io/badge/license-AGPLv3%2B-purple)][LICENSE]
[![build status](https://gitlab.com/zetok/ods2sql/badges/master/pipeline.svg)](https://gitlab.com/zetok/ods2sql/commits/master)

**ods2sql** creates [SQLite] database out of your ODS spreadsheet.

Details:

- Cell values in the first row are used for SQLite column names.
- To simplify things, all data from spreadsheet is treated as `TEXT` SQLite
  type.
  - Boolean cell values (e.g. `true/false`) are converted to `TEXT` type and
    saved as `true` / `false` text.
- Empty cells/errors are inserted as `NULL` SQLite value.
- Maximum number of columns is limited by:
  - [`SQLITE_LIMIT_VARIABLE_NUMBER`] to **999** parameters in `INSERT`
    statements.
  - [`SQLITE_MAX_COLUMN`] to **2000** columns in a table.
  - LibreOffice has a hardcoded [limit of **1024** columns].


## Caution!

It works for me, and if it eats your data that's your problem. Read the
[LICENSE].

Patches for bugs are welcome.

## Usage

To create `spreadsheet.sqlite` out of `spreadsheet.ods`:

```bash
ods2sql spreadsheet.ods
```

## Install

The easiest way is to use [`Cargo`]:

```bash
cargo install ods2sql
```

Don't forget to add place where Cargo installs binaries to your `PATH` for
convenience, e.g.:

```bash
echo 'export PATH="$PATH:$HOME/.cargo/bin"' >> ~/.bashrc
source ~/.bashrc
```

## Updating

In case of newer version, updating is almost like installing:

```bash
cargo install --force ods2sql
```

## License

Licensed under AGPLv3+. For details, see [LICENSE].


[LICENSE]: /LICENSE
[Rust]: https://www.rust-lang.org/
[`SQLITE_LIMIT_VARIABLE_NUMBER`]: https://www.sqlite.org/limits.html#max_variable_number
[`SQLITE_MAX_COLUMN`]: https://www.sqlite.org/limits.html#max_column
[SQLite]: https://www.sqlite.org
[`Cargo`]: https://doc.rust-lang.org/cargo/
[limit of **1024** columns]: https://bugs.documentfoundation.org/show_bug.cgi?id=50916
